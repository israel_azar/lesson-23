﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer israel = new Customer(567, "Israel", 1994, "Kiryat Malachi", 1);
            Customer lior = new Customer(689, "Lior", 1997, "Ramla", 10);
            Customer naor = new Customer(123, "Naor", 1995, "Netanya", 5);
            Customer ariel = new Customer(947, "Ariel", 1980, "New York", 8);

            MyQueue my = new MyQueue();
            my.Enqueue(israel);
            my.Enqueue(lior);
            my.Enqueue(naor);
            my.Enqueue(ariel);
            Console.WriteLine(my.DequeueProtectiza());
            Console.WriteLine(my);
        }
    }
}
