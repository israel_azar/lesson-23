﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class MyQueue
    {
        private List<Customer> customers = new List<Customer>();

        public void Enqueue (Customer customer)
        {
            customers.Add(customer);
        }
        public Customer Dequeue ()
        {
            Customer customer = customers[0];
            customers.RemoveAt(0);
            return customer;
        }
        public void Init (List<Customer> new_customers)
        {
            customers = new_customers;
        }
        public void Clear ()
        {
            customers.Clear();
        }
        public Customer WhoIsNext ()
        {
            return customers[0];
        }
        public int Count
        {
            get { return customers.Count(); }
        }
        public void SortByProtection ()
        {
            for (int i = 0; i < customers.Count - 1; i++)
            {
                for (int j =  i + 1; j < customers.Count; j++)
                {
                    if (customers[j].Protection > customers[i].Protection)
                    {
                        Customer change = customers[i];
                        customers[i] = customers[j];
                        customers[j] = change;
                    }
                }
            }
        }
        public void SortByTotalPurchases ()
        {
            for (int i = 0; i < customers.Count - 1; i++)
            {
                for (int j = i + 1; j < customers.Count; j++)
                {
                    if (customers[j].Total_Purchases > customers[i].Total_Purchases)
                    {
                        Customer change = customers[i];
                        customers[i] = customers[j];
                        customers[j] = change;
                    }
                }
            }
        }
        public void SortByBirthYear ()
        {
            for (int i = 0; i < customers.Count - 1; i++)
            {
                for (int j = i + 1; j < customers.Count; j++)
                {
                    if (customers[j].Birth_Year < customers[i].Birth_Year)
                    {
                        Customer change = customers[i];
                        customers[i] = customers[j];
                        customers[j] = change;
                    }
                }
            }
        }
        public List<Customer> DequeueCustomers (int Num)
        {
            customers.RemoveRange(0, Num);
            List<Customer> new_customers = new List<Customer>();
            for (int i = 0; i < Num - 1; i++)
            {
                Customer add = customers[i];
                new_customers.Add(add);
            }
            return new_customers;
        }
        public void AniRakSheela (Customer customer)
        {
            for (int i = 0; i < customers.Count; i++)
            {
                if (customer == customers[i])
                {
                    customers.Remove(customers[i]);
                }
            }
            customers.Insert(0, customer);
        }
        public Customer DequeueProtectiza()
        {
            Customer biggest_prot = customers[0];
            for (int i = 1; i < customers.Count; i++)
            {
                if (customers[i].Protection > biggest_prot.Protection)
                {
                    biggest_prot = customers[i];
                }
            }
            for (int i = 0; i < customers.Count; i++)
            {
                if (biggest_prot.Protection == customers[i].Protection)
                {
                    customers.Remove(customers[i]);
                    break;
                }
            }
            return biggest_prot;
        }
    }
}
