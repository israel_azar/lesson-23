﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_23
{
    class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Birth_Year { get; set; }
        public string Adress { get; set; }
        public int Protection { get; set; }
        public int Total_Purchases { get; set; }

        public Customer(int id, string name, int birth_Year, string adress, int protection)
        {
            Id = id;
            Name = name;
            Birth_Year = birth_Year;
            Adress = adress;
            Protection = protection;
        }

        public override string ToString()
        {
            return "Customer Id: " + Id + " Customer Name: " + Name + " Customer Birth Year: " + Birth_Year + " Customer Adress: " + Adress + " Customer Protection: " + Protection;
        }
    }
}
